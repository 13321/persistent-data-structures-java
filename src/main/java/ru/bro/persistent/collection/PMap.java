package ru.bro.persistent.collection;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import ru.bro.persistent.collection.version.VersionedMap;

public abstract class PMap<Key, Value> extends VersionedMap<Key, Value> implements Map<Key, Value> {
  public PMap() {

  }

  @Override
  public boolean undo() {
    return versionManager.undo();
  }

  @Override
  public boolean redo() {
    return versionManager.redo();
  }

  @Override
  public void clear() {
    super.vclear();
  }

  @Override
  public boolean containsKey(Object key) {
    return getAliveMap().containsKey(key);
  }

  @Override
  public boolean containsValue(Object value) {
    return getAliveMap().containsValue(value);
  }

  @Override
  public Set<Entry<Key, Value>> entrySet() {
    return getAliveMap().entrySet();
  }

  @Override
  public Value get(Object key) {
    return getAliveMap().get(key);
  }

  @Override
  public boolean isEmpty() {
    return getAliveMap().isEmpty();
  }

  @Override
  public Set<Key> keySet() {
    return getAliveMap().keySet();
  }

  @Override
  public Value put(Key key, Value value) {
    super.vadd(key, value);
    return value;
  }

  @Override
  public void putAll(Map<? extends Key, ? extends Value> map) {
    super.vaddAll(map);
  }

  @Override
  @SuppressWarnings("unchecked")
  public Value remove(Object key) {
    Value value = getAliveMap().get(key);
    super.vremove((Key) key);
    return value;
  }

  @Override
  public int size() {
    return getAliveMap().size();
  }

  @Override
  public Collection<Value> values() {
    return getAliveMap().values();
  }
}
