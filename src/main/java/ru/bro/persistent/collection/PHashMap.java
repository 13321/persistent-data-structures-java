package ru.bro.persistent.collection;

import java.util.HashMap;

public class PHashMap<Key, Value> extends PMap<Key, Value> {
  public PHashMap() {
    setAliveMap(new HashMap<Key, Value>());
  }

  @Override
  public String toString() {
    return "PHashMap " + getAliveMap().toString();
  }
}
