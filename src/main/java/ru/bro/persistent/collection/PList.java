package ru.bro.persistent.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import ru.bro.persistent.collection.version.VersionedList;

/**
 * @author Bro
 * @param <Type>
 */
public abstract class PList<Type> extends VersionedList<Type> implements List<Type> {

  public PList() {
    
  }

  @Override
  public boolean undo() {
    return versionManager.undo();
  }

  @Override
  public boolean redo() {
    return versionManager.redo();
  }
  
  @Override
  public boolean add(Type e) {
    super.vadd(e);
    return true;
  }

  @Override
  public void add(int index, Type element) {
    super.vadd(index, element);
  }

  @Override
  public boolean addAll(Collection<? extends Type> c) {
    super.vaddAll(c);
    return true;
  }

  @Override
  public boolean addAll(int index, Collection<? extends Type> c) {
    super.vaddAll(index, c);
    return true;
  }

  @Override
  public void clear() {
    super.vclear();
  }

  @Override
  public boolean contains(Object o) {
    return getAliveList().contains(o);
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return getAliveList().containsAll(c);
  }

  @Override
  public Type get(int index) {
    return getAliveList().get(index);
  }

  @Override
  public int indexOf(Object o) {
    return getAliveList().indexOf(o);
  }

  @Override
  public boolean isEmpty() {
    return getAliveList().isEmpty();
  }

  @Override
  public Iterator<Type> iterator() {
    return getAliveList().iterator();
  }

  @Override
  public int lastIndexOf(Object o) {
    return getAliveList().lastIndexOf(o);
  }

  @Override
  public ListIterator<Type> listIterator() {
    return getAliveList().listIterator();
  }

  @Override
  public ListIterator<Type> listIterator(int index) {
    return getAliveList().listIterator(index);
  }

  @Override
  public boolean remove(Object o) {
    int index = getAliveList().indexOf(o);
    if (index < 0) {
      return false;
    }
    
    super.vremove(index);
    return true;
  }

  @Override
  public Type remove(int index) {
    if (index < 0 || index >= getAliveList().size()) {
      return null;
    }
    
    return super.vremove(index);
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type set(int index, Type element) {
    if (index < 0 || index >= getAliveList().size()) {
      return null;
    }
    
    super.vreplace(index, element);
    return element;
  }

  @Override
  public int size() {
    return getAliveList().size();
  }

  @Override
  public Object[] toArray() {
    return getAliveList().toArray();
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return getAliveList().toArray(a);
  }

}
