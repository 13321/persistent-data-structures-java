package ru.bro.persistent.collection;

import java.util.TreeMap;

public class PTreeMap<Key, Value> extends PMap<Key, Value> {
  public PTreeMap() {
    setAliveMap(new TreeMap<Key, Value>());
  }

  @Override
  public String toString() {
    return "PTreeMap " + getAliveMap().toString();
  }
}
