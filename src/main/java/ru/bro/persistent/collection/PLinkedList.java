package ru.bro.persistent.collection;

import java.util.LinkedList;

public class PLinkedList<Type> extends PList<Type> {
  public PLinkedList() {
    setAliveList(new LinkedList<Type>());
  }

  @Override
  public PLinkedList<Type> subList(int fromIndex, int toIndex) {
    PLinkedList<Type> subList = new PLinkedList<Type>();
    subList.addAll(getAliveList().subList(fromIndex, toIndex));
    return subList;
  }

  @Override
  public String toString() {
    return "PLinkedList " + getAliveList().toString();
  }
}
