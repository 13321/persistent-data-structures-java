package ru.bro.persistent.collection;

import java.util.ArrayList;

public class PArrayList<Type> extends PList<Type> {
  public PArrayList() {
    setAliveList(new ArrayList<Type>());
  }

  @Override
  public PArrayList<Type> subList(int fromIndex, int toIndex) {
    PArrayList<Type> subList = new PArrayList<Type>();
    subList.addAll(getAliveList().subList(fromIndex, toIndex));
    return subList;
  }

  @Override
  public String toString() {
    return "PArrayList " + getAliveList().toString();
  }
}
