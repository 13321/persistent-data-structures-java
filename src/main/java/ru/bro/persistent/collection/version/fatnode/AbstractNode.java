package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 */
public abstract class AbstractNode {
  private AbstractNode next;
  private AbstractNode previous;

  public AbstractNode() {

  }

  public AbstractNode(AbstractNode previous, AbstractNode next) {
    this.next = next;
    this.previous = previous;
  }

  public AbstractNode getNext() {
    return next;
  }

  public void setNext(AbstractNode next) {
    this.next = next;
  }

  public AbstractNode getPrevious() {
    return previous;
  }

  public void setPrevious(AbstractNode previous) {
    this.previous = previous;
  }
}
