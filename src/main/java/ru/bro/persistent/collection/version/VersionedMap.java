package ru.bro.persistent.collection.version;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ru.bro.persistent.collection.version.fatnode.FatNode;

/**
 * @author Bro
 * @param <Key>
 * @param <Value>
 */
public abstract class VersionedMap<Key, Value> extends VersionedCollection {
  private Map<Key, FatNode<Value>> map;
  private Map<Key, Value> aliveMap;

  public VersionedMap() {
    map = new HashMap<Key, FatNode<Value>>();
    aliveMap = new HashMap<Key, Value>();
  }

  protected final void setVersion(long version) {
    for (FatNode<Value> fatNode : map.values()) {
      fatNode.setVersion(version);
    }
  }

  protected final void updateAlive() {
    aliveMap = new HashMap<Key, Value>();
    for (Key key : map.keySet()) {
      FatNode<Value> fatNode = map.get(key);
      if (!fatNode.isValue()) {
        continue;
      }

      aliveMap.put(key, fatNode.getValue());
    }
  }

  public void narrow(long version) {
    List<Key> lKeysToRemove = new LinkedList<Key>();
    for (Key key : map.keySet()) {
      FatNode<Value> fatNode = map.get(key);
      fatNode.narrow(version);
      if (fatNode.isBottom()) {
        lKeysToRemove.add(key);
      }
    }

    for (Key key : lKeysToRemove) {
      map.remove(key);
    }
  }

  protected Map<Key, Value> getAliveMap() {
    return aliveMap;
  }

  protected void setAliveMap(Map<Key, Value> map) {
    aliveMap = map;
  }

  protected void vadd(Key key, Value value) {
    long version = versionManager.addVersion(this);

    if (aliveMap.containsKey(key)) {
      FatNode<Value> fatNode = map.get(key);
      fatNode.add(value, version);
    } else {
      FatNode<Value> fatNode = new FatNode<Value>();
      fatNode.add(value, version);
      map.put(key, fatNode);
    }

    if (value instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) value).getVersionManager());
      ((VersionedCollection) value).setVersionManager(versionManager);
    }

    updateAlive();
  }

  protected void vaddAll(Map<? extends Key, ? extends Value> map) {
    long version = versionManager.addVersion(this);

    for (Key key : map.keySet()) {
      if (aliveMap.containsKey(key)) {
        FatNode<Value> fatNode = this.map.get(key);
        fatNode.add(map.get(key), version);
      } else {
        FatNode<Value> fatNode = new FatNode<Value>();
        fatNode.add(map.get(key), version);
        this.map.put(key, fatNode);
      }
    }

    updateAlive();
  }

  protected void vremove(Key key) {
    long version = versionManager.addVersion(this);

    map.get(key).remove(version);
    aliveMap.remove(key);
  }

  protected void vclear() {
    long version = versionManager.addVersion(this);

    for (Key key : map.keySet()) {
      map.get(key).remove(version);
    }

    aliveMap.clear();
  }

  @Override
  public void setVersionManager(VersionManager versionManager) {
    super.setVersionManager(versionManager);
    for (Key key : map.keySet()) {
      FatNode<Value> fatNode = map.get(key);
      if (!fatNode.isValue()) {
        continue;
      }

      Value val = fatNode.getValue();
      if (val instanceof VersionedCollection) {
        ((VersionedCollection) val).setVersionManager(versionManager);
      }
    }
  }
}
