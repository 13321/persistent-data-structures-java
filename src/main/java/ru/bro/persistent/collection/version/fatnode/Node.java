package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 * @param <Type>
 */
public final class Node<Type> extends VersionedNode {
  private Type value;

  public Node(Type value, long version) {
    super(version);
    this.value = value;
  }

  public Node(Type value, long version, AbstractNode previous, AbstractNode next) {
    super(version, previous, next);
    this.value = value;
  }

  public Type getValue() {
    return value;
  }

  public void setValue(Type value) {
    this.value = value;
  }
}
