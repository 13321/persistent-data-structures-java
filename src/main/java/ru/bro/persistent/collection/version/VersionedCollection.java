package ru.bro.persistent.collection.version;

/**
 * @author Bro
 */
public abstract class VersionedCollection implements Persistent {
  protected VersionManager versionManager;

  public VersionedCollection() {
    versionManager = new VersionManager();
  }

  public VersionManager getVersionManager() {
    return versionManager;
  }

  public void setVersionManager(VersionManager versionManager) {
    this.versionManager = versionManager;
  }

  public boolean internalUndo(long version) {
    setVersion(version);
    updateAlive();
    return true;
  }

  public boolean internalRedo(long version) {
    setVersion(version);
    updateAlive();
    return true;
  }

  protected abstract void setVersion(long version);

  protected abstract void updateAlive();

  public abstract void narrow(long version);

  public long getVersion() {
    return versionManager.getVersion();
  }
}
