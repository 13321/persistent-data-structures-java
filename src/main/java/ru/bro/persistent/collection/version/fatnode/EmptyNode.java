package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 */
public final class EmptyNode extends VersionedNode {
  public EmptyNode(long version) {
    super(version);
  }

  public EmptyNode(long version, AbstractNode previous, AbstractNode next) {
    super(version, previous, next);
  }
}
