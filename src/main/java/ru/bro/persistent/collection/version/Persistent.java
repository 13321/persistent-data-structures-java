package ru.bro.persistent.collection.version;

/**
 * @author bro
 */
public interface Persistent {
  public boolean undo();

  public boolean redo();
}
