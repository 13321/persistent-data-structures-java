package ru.bro.persistent.collection.version;

import ru.bro.persistent.collection.version.fatnode.AbstractNode;
import ru.bro.persistent.collection.version.fatnode.BottomNode;
import ru.bro.persistent.collection.version.fatnode.FatNode;
import ru.bro.persistent.collection.version.fatnode.Node;

/**
 * @author bro
 */
public class VersionManager {
  private FatNode<VersionedCollection> node;

  public VersionManager() {
    node = new FatNode<VersionedCollection>();
  }

  @SuppressWarnings("rawtypes")
  public void merge(VersionManager versionManager) {
    if (this == versionManager) {
      return;
    }

    AbstractNode tempNode = versionManager.node.currentNode();
    while (tempNode.getPrevious() != null) {
      tempNode = tempNode.getPrevious();
    }

    if (tempNode instanceof BottomNode) {
      tempNode = tempNode.getNext();
    }

    long curVersion = -2;
    while (tempNode != null) {
      if (tempNode instanceof Node) {
        Object collection = ((Node) tempNode).getValue();
        if (collection instanceof VersionedCollection) {
          long v = addVersion((VersionedCollection) collection);
          if (tempNode == versionManager.node.currentNode()) {
            curVersion = v - 1;
          }
        }
      }

      tempNode = tempNode.getNext();
    }

    if (curVersion > -2) {
      node.setVersion(curVersion);
    }
  }

  public boolean hasNextVersion() {
    return node.hasNextVersion();
  }

  public boolean hasPreviousVersion() {
    return node.hasPreviousVersion();
  }

  public boolean undo() {
    if (!node.isValue()) {
      return false;
    }

    node.getValue().internalUndo(node.getPreviousVersion());
    node.setVersion(node.getPreviousVersion());
    return true;
  }

  public boolean redo() {
    if (!node.hasNextVersion()) {
      return false;
    }

    node.setVersion(node.getNextVersion());
    node.getValue().internalRedo(node.getVersion());
    return true;
  }

  @SuppressWarnings("rawtypes")
  public long addVersion(VersionedCollection collection) {
    long version = node.hasVersion() ? node.getVersion() : -1;
    if (hasNextVersion()) {
      AbstractNode curNode = node.currentNode();
      AbstractNode lastNode = node.lastNode();
      while (lastNode != curNode) {
        if (lastNode instanceof Node) {
          ((VersionedCollection) ((Node) lastNode).getValue()).narrow(version);
        }
        lastNode = lastNode.getPrevious();
      }
    }

    node.add(collection, ++version);
    return version;
  }

  public long getVersion() {
    return node.getVersion();
  }
}
