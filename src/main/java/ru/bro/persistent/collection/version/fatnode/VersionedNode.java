package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 */
public abstract class VersionedNode extends AbstractNode {
  private final long version;

  public VersionedNode(long version) {
    super();
    this.version = version;
  }

  public VersionedNode(long version, AbstractNode previous, AbstractNode next) {
    super(previous, next);
    this.version = version;
  }

  public long getVersion() {
    return version;
  }
}
