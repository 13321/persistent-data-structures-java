package ru.bro.persistent.collection.version;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import ru.bro.persistent.collection.version.fatnode.FatNode;

/**
 * @author Bro
 * @param <Type>
 */
public abstract class VersionedList<Type> extends VersionedCollection {
  private List<FatNode<Type>> list;
  private List<Type> aliveList;
  private Map<Integer, Integer> aliveToSourceIndexMap;

  public VersionedList() {
    list = new LinkedList<FatNode<Type>>();
    aliveToSourceIndexMap = new HashMap<Integer, Integer>();
  }

  protected void setVersion(long version) {
    for (FatNode<Type> fatNode : list) {
      fatNode.setVersion(version);
    }
  }

  protected void updateAlive() {
    aliveList = new LinkedList<Type>();
    int index = -1;
    for (FatNode<Type> fatNode : list) {
      ++index;
      if (!fatNode.isValue()) {
        continue;
      }

      aliveToSourceIndexMap.put(aliveList.size(), index);

      aliveList.add(fatNode.getValue());
    }
  }

  public void narrow(long version) {
    Predicate<FatNode<Type>> predicate = (FatNode<Type> fatNode) -> {
      fatNode.narrow(version);
      return fatNode.isBottom();
    };
    list.removeIf(predicate);
  }

  protected List<Type> getAliveList() {
    return aliveList;
  }

  protected void setAliveList(List<Type> list) {
    aliveList = list;
  }

  public void vadd(Type value) {
    long version = versionManager.addVersion(this);

    FatNode<Type> fatNode = new FatNode<Type>();
    fatNode.add(value, version);
    list.add(fatNode);

    if (value instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) value).getVersionManager());
      ((VersionedCollection) value).setVersionManager(versionManager);
    }

    updateAlive();
  }

  public void vadd(int index, Type value) {
    long version = versionManager.addVersion(this);

    updateAlive();

    int fatIndex = 1 + aliveToSourceIndexMap.get(index - 1);

    FatNode<Type> fatNode = new FatNode<Type>();
    fatNode.add(value, version);

    list.add(fatIndex, fatNode);

    if (value instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) value).getVersionManager());
      ((VersionedCollection) value).setVersionManager(versionManager);
    }

    aliveList.add(index, value);
  }

  public void vaddAll(Collection<? extends Type> collection) {
    long version = versionManager.addVersion(this);

    for (Type value : collection) {
      FatNode<Type> fatNode = new FatNode<Type>();
      fatNode.add(value, version);
      list.add(fatNode);
    }

    if (collection instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) collection).getVersionManager());
      ((VersionedCollection) collection).setVersionManager(versionManager);
    }

    updateAlive();
  }

  public void vaddAll(int index, Collection<? extends Type> collection) {
    long version = versionManager.addVersion(this);

    updateAlive();

    int fatIndex = 1 + aliveToSourceIndexMap.get(index - 1);

    for (Type value : collection) {
      FatNode<Type> fatNode = new FatNode<Type>();
      fatNode.add(value, version);
      list.add(fatIndex, fatNode);

      ++fatIndex;
    }

    if (collection instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) collection).getVersionManager());
      ((VersionedCollection) collection).setVersionManager(versionManager);
    }

    aliveList.addAll(index, collection);
  }

  public Type vremove(int index) {
    int fatIndex = aliveToSourceIndexMap.get(index);

    long version = versionManager.addVersion(this);

    FatNode<Type> fatNode = list.get(fatIndex);
    Type value = fatNode.getValue();
    fatNode.remove(version);

    updateAlive();
    return value;
  }

  public void vreplace(int index, Type value) {
    int fatIndex = aliveToSourceIndexMap.get(index);

    long version = versionManager.addVersion(this);

    if (value instanceof VersionedCollection) {
      versionManager.merge(((VersionedCollection) value).getVersionManager());
      ((VersionedCollection) value).setVersionManager(versionManager);
    }

    FatNode<Type> fatNode = list.get(fatIndex);
    fatNode.modify(value, version);

    updateAlive();
  }

  public void vclear() {
    long version = versionManager.addVersion(this);

    for (FatNode<Type> fatNode : list) {
      if (!fatNode.isEmpty() && !fatNode.isBottom()) {
        fatNode.remove(version);
      }
    }

    aliveList.clear();
  }

  @Override
  public void setVersionManager(VersionManager versionManager) {
    super.setVersionManager(versionManager);
    for (FatNode<Type> fatNode : list) {
      if (!fatNode.isValue()) {
        continue;
      }

      Type val = fatNode.getValue();
      if (val instanceof VersionedCollection) {
        ((VersionedCollection) val).setVersionManager(versionManager);
      }
    }
  }
}
