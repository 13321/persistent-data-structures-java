package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 */
public final class BottomNode extends AbstractNode {
  public BottomNode() {
    super();
  }

  public BottomNode(AbstractNode next) {
    super(null, next);
  }
}
