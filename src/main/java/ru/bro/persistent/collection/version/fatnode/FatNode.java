package ru.bro.persistent.collection.version.fatnode;

/**
 * @author Bro
 * @param <Type>
 */
public final class FatNode<Type> {
  private AbstractNode node;

  public FatNode() {
    node = new BottomNode();
  }

  @SuppressWarnings("unchecked")
  public Type getValue() {
    Type value = null;
    try {
      if (node instanceof Node) {
        Node<Type> valueNode = (Node<Type>) Node.class.cast(node);
        value = valueNode.getValue();
      }
    } catch (Exception e) {
      value = null;
    }
    return value;
  }

  public long getVersion() {
    long version = -1;
    try {
      VersionedNode versionedNode = (VersionedNode) VersionedNode.class.cast(node);
      version = versionedNode.getVersion();
    } catch (Exception e) {
      version = -1;
    }
    return version;
  }

  public boolean hasVersion() {
    return getVersion() >= 0;
  }

  public long getNextVersion() {
    long version = -1;
    try {
      VersionedNode versionedNode = (VersionedNode) VersionedNode.class.cast(node.getNext());
      version = versionedNode.getVersion();
    } catch (Exception e) {
      version = -1;
    }
    return version;
  }

  public boolean hasNextVersion() {
    return getNextVersion() > 0;
  }

  public long getPreviousVersion() {
    long version = -1;
    try {
      VersionedNode versionedNode = (VersionedNode) VersionedNode.class.cast(node.getPrevious());
      version = versionedNode.getVersion();
    } catch (Exception e) {
      version = -1;
    }
    return version;
  }

  public boolean hasPreviousVersion() {
    return getPreviousVersion() > 0;
  }

  public void add(Type value, long version) {
    node.setNext(new Node<Type>(value, version, node, null));
    node = node.getNext();
  }

  public void modify(Type value, long version) {
    add(value, version);
  }

  public void remove(long version) {
    node.setNext(new EmptyNode(version, node, null));
    node = node.getNext();
  }

  private boolean isSatisfiedVersion(long desiredVersion) {
    long version = getVersion();
    long nextVersion = getNextVersion();

    boolean hasVersion = version > 0;
    boolean hasNextVersion = nextVersion > 0;

    if (!hasVersion) {
      if (!hasNextVersion) {
        return true;
      }

      return nextVersion <= desiredVersion ? false : true;
    }

    if (version == desiredVersion) {
      return true;
    }

    if (version > desiredVersion) {
      return false;
    }

    return hasNextVersion ? (nextVersion > desiredVersion) : true;
  }

  public void setVersion(long version) {
    while (!isSatisfiedVersion(version)) {
      node = getVersion() > version ? node.getPrevious() : node.getNext();
    }
  }

  public boolean isValue() {
    return node instanceof Node;
  }

  public boolean isEmpty() {
    return node instanceof EmptyNode;
  }

  public boolean isBottom() {
    return node instanceof BottomNode;
  }

  public boolean isVersioned() {
    return node instanceof VersionedNode;
  }

  public void narrow(long version) {
    setVersion(version);
    if (node != null) {
      node.setNext(null);
    }
  }

  public AbstractNode currentNode() {
    return node;
  }

  public AbstractNode lastNode() {
    AbstractNode lastNode = node;
    while (lastNode.getNext() != null) {
      lastNode = lastNode.getNext();
    }
    return lastNode;
  }
}
