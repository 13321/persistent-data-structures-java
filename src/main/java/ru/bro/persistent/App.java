package ru.bro.persistent;

import ru.bro.persistent.collection.PHashMap;
import ru.bro.persistent.collection.PLinkedList;
import ru.bro.persistent.collection.PList;
import ru.bro.persistent.collection.PMap;

public class App {
  public static void main(String[] args) {
    testList();
    testMap();
  }

  public static void testList() {
    PList<Character> arr = new PLinkedList<Character>();

    arr.add('g');
    System.out.println(arr.toString());

    arr.add('o');
    System.out.println(arr.toString());

    arr.add('v');
    System.out.println(arr.toString());

    arr.add('n');
    System.out.println(arr.toString());

    arr.add('o');
    System.out.println(arr.toString());

    arr.add('!');
    System.out.println(arr.toString());

    arr.remove(5);
    System.out.println(arr.toString());

    arr.remove(0);
    System.out.println(arr.toString());

    arr.undo();
    System.out.println(arr.toString());

    arr.undo();
    System.out.println(arr.toString());

    arr.redo();
    System.out.println(arr.toString());
  }

  public static void testMap() {
    PMap<String, String> map = new PHashMap<String, String>();

    map.put("г", "грецкий");
    System.out.println(map.toString());

    map.put("о", "орех");
    System.out.println(map.toString());

    map.put("в", "вкусный");
    System.out.println(map.toString());

    map.put("н", "ночью");
    System.out.println(map.toString());

    map.put("о", "очень");
    System.out.println(map.toString());

    map.undo();
    System.out.println(map.toString());

    map.put("!", "!");
    System.out.println(map.toString());

    map.remove("о");
    System.out.println(map.toString());

    map.remove("в");
    System.out.println(map.toString());

    map.undo();
    System.out.println(map.toString());

    map.undo();
    System.out.println(map.toString());

    map.redo();
    System.out.println(map.toString());
  }
}
