package ru.bro.persistent;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ru.bro.persistent.collection.PLinkedList;
import ru.bro.persistent.collection.PList;

import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class PLinkedListTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName
   *          name of the test case
   */
  public PLinkedListTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(PLinkedListTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testPLinkedList() {
    assertTrue(true);
  }

  public void testAdd() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
  }

  public void testSize() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    assertEquals(1, arr.size());
  }

  public void testClear() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    arr.clear();
    assertEquals(0, arr.size());
  }

  public void testGet() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    assertEquals('a', (char) arr.get(0));
  }

  public void testToSting() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    assertEquals(arr.toString(), "PLinkedList [a]");
  }

  public void testUndo() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    arr.add('b');
    arr.undo();
    assertEquals(1, arr.size());
    assertEquals(arr.toString(), "PLinkedList [a]");
  }

  public void testRedo() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    arr.add('b');
    arr.undo();
    arr.redo();
    assertEquals(2, arr.size());
    assertEquals(arr.toString(), "PLinkedList [a, b]");
  }

  public void testAddAll() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    arr.add('b');
    arr.addAll(Arrays.asList('c', 'd'));
    assertEquals(4, arr.size());
    assertEquals(arr.toString(), "PLinkedList [a, b, c, d]");
  }

  public void testAddAllUndoRedo() {
    PList<Character> arr = new PLinkedList<Character>();
    arr.add('a');
    arr.add('b');
    arr.addAll(Arrays.asList('c', 'd'));
    arr.undo();
    assertEquals(2, arr.size());
    assertEquals(arr.toString(), "PLinkedList [a, b]");
    arr.redo();
    assertEquals(4, arr.size());
    assertEquals(arr.toString(), "PLinkedList [a, b, c, d]");
  }
}
