package ru.bro.persistent;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ru.bro.persistent.collection.PArrayList;
import ru.bro.persistent.collection.PHashMap;
import ru.bro.persistent.collection.PLinkedList;
import ru.bro.persistent.collection.PList;
import ru.bro.persistent.collection.PMap;
import ru.bro.persistent.collection.PTreeMap;

/**
 * Unit test for simple App.
 */
public class CascadeTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName
   *          name of the test case
   */
  public CascadeTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(CascadeTest.class);
  }

  public void testCascade() {
    PList<PList<PMap<String, String>>> cascade = new PLinkedList<PList<PMap<String, String>>>();
    assertEquals(cascade.getVersion(), -1);

    PList<PMap<String, String>> cascade1 = new PArrayList<PMap<String, String>>();
    assertEquals(cascade1.getVersion(), -1);
    PMap<String, String> map1 = new PHashMap<String, String>();
    assertEquals(map1.getVersion(), -1);
    PMap<String, String> map2 = new PTreeMap<String, String>();
    assertEquals(map2.getVersion(), -1);
    cascade1.add(map1);
    assertEquals(cascade1.getVersion(), 0);
    assertEquals(cascade1.toString(), "PArrayList [PHashMap {}]");
    cascade1.add(map2);
    assertEquals(cascade1.getVersion(), 1);
    assertEquals(cascade1.toString(), "PArrayList [PHashMap {}, PTreeMap {}]");

    PList<PMap<String, String>> cascade2 = new PLinkedList<PMap<String, String>>();
    assertEquals(cascade2.getVersion(), -1);
    PMap<String, String> map3 = new PHashMap<String, String>();
    assertEquals(map3.getVersion(), -1);
    PMap<String, String> map4 = new PTreeMap<String, String>();
    assertEquals(map4.getVersion(), -1);
    cascade2.add(map3);
    assertEquals(cascade2.getVersion(), 0);
    assertEquals(cascade2.toString(), "PLinkedList [PHashMap {}]");
    cascade2.add(map4);
    assertEquals(cascade2.getVersion(), 1);
    assertEquals(cascade2.toString(), "PLinkedList [PHashMap {}, PTreeMap {}]");

    PList<PMap<String, String>> cascade3 = new PArrayList<PMap<String, String>>();
    assertEquals(cascade3.getVersion(), -1);
    PMap<String, String> map5 = new PHashMap<String, String>();
    assertEquals(map5.getVersion(), -1);
    PMap<String, String> map6 = new PTreeMap<String, String>();
    assertEquals(map6.getVersion(), -1);
    cascade3.add(map5);
    assertEquals(cascade3.getVersion(), 0);
    assertEquals(cascade3.toString(), "PArrayList [PHashMap {}]");
    cascade3.add(map6);
    assertEquals(cascade3.getVersion(), 1);
    assertEquals(cascade3.toString(), "PArrayList [PHashMap {}, PTreeMap {}]");

    cascade.add(cascade1);
    assertEquals(cascade.getVersion(), 1);
    assertEquals(cascade.toString(), "PLinkedList [PArrayList [PHashMap {}, PTreeMap {}]]");
    cascade.add(cascade2);
    assertEquals(cascade.getVersion(), 3);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {}, PTreeMap {}], PLinkedList [PHashMap {}, PTreeMap {}]]");
    cascade.add(cascade3);
    assertEquals(cascade.getVersion(), 5);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {}, PTreeMap {}], PLinkedList [PHashMap {}, PTreeMap {}], PArrayList [PHashMap {}, PTreeMap {}]]");

    map1.put("1", "1");
    assertEquals(cascade.getVersion(), 6);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {}], PLinkedList [PHashMap {}, PTreeMap {}], PArrayList [PHashMap {}, PTreeMap {}]]");

    map2.put("2", "2");
    assertEquals(cascade.getVersion(), 7);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {2=2}], PLinkedList [PHashMap {}, PTreeMap {}], PArrayList [PHashMap {}, PTreeMap {}]]");

    map3.put("3", "3");
    assertEquals(cascade.getVersion(), 8);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {}], PArrayList [PHashMap {}, PTreeMap {}]]");

    map4.put("4", "4");
    assertEquals(cascade.getVersion(), 9);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {}, PTreeMap {}]]");

    map5.put("5", "5");
    assertEquals(cascade.getVersion(), 10);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {}]]");

    map6.put("6", "6");
    assertEquals(cascade.getVersion(), 11);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    map1.put("7", "7");
    assertEquals(cascade.getVersion(), 12);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    map2.put("8", "8");
    assertEquals(cascade.getVersion(), 13);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    map3.put("9", "9");
    assertEquals(cascade.getVersion(), 14);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    map4.put("10", "10");
    assertEquals(cascade.getVersion(), 15);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    map5.put("11", "11");
    assertEquals(cascade.getVersion(), 16);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {11=11, 5=5}, PTreeMap {6=6}]]");

    map6.put("12", "12");
    assertEquals(cascade.getVersion(), 17);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {11=11, 5=5}, PTreeMap {12=12, 6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 16);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {11=11, 5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 15);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 14);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.redo();
    assertEquals(cascade.getVersion(), 15);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.redo();
    assertEquals(cascade.getVersion(), 16);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {11=11, 5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 15);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4, 10=10}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 14);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3, 9=9}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 13);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2, 8=8}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.undo();
    assertEquals(cascade.getVersion(), 12);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");

    cascade.clear();
    assertEquals(cascade.getVersion(), 13);
    assertEquals(cascade.toString(), "PLinkedList []");

    cascade.undo();
    assertEquals(cascade.getVersion(), 12);
    assertEquals(cascade.toString(),
        "PLinkedList [PArrayList [PHashMap {1=1, 7=7}, PTreeMap {2=2}], PLinkedList [PHashMap {3=3}, PTreeMap {4=4}], PArrayList [PHashMap {5=5}, PTreeMap {6=6}]]");
  }

  public void testPreHistory() {
    PList<PList<Integer>> list1 = new PArrayList<PList<Integer>>();
    PList<Integer> list2 = new PLinkedList<Integer>();
    list2.add(1);
    list2.add(2);
    list2.add(3);
    list2.add(4);
    list2.add(5);
    list2.add(6);
    list2.undo();
    list2.undo();
    list2.undo();
    list1.add(list2);
    assertEquals(list1.toString(), "PArrayList [PLinkedList [1, 2, 3]]");
    list1.redo();
    assertEquals(list1.toString(), "PArrayList [PLinkedList [1, 2, 3, 4]]");
    list1.redo();
    assertEquals(list1.toString(), "PArrayList [PLinkedList [1, 2, 3, 4, 5]]");
    assertEquals(list2.toString(), "PLinkedList [1, 2, 3, 4, 5]");

  }
}
