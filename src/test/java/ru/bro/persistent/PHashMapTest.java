package ru.bro.persistent;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ru.bro.persistent.collection.PHashMap;
import ru.bro.persistent.collection.PMap;

import java.util.Map;
import java.util.HashMap;

/**
 * Unit test for simple App.
 */
public class PHashMapTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName
   *          name of the test case
   */
  public PHashMapTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(PHashMapTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testPHashMap() {
    assertTrue(true);
  }

  public void testAdd() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
  }

  public void testSize() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    assertEquals(1, map.size());
  }

  public void testClear() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    map.clear();
    assertEquals(0, map.size());
  }

  public void testGet() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    assertEquals("1", map.get("a"));
  }

  public void testToSting() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    System.out.println(map.toString());
    assertEquals(map.toString(), "PHashMap {a=1}");
  }

  public void testUndo() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    map.put("b", "2");
    map.undo();
    assertEquals(1, map.size());
    assertEquals(map.toString(), "PHashMap {a=1}");
  }

  public void testRedo() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    map.put("b", "2");
    map.undo();
    map.redo();
    assertEquals(2, map.size());
    assertEquals(map.toString(), "PHashMap {a=1, b=2}");
  }

  public void testPutAll() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    map.put("b", "2");
    Map<String, String> tmp = new HashMap<String, String>();
    tmp.put("c", "3");
    tmp.put("d", "4");
    map.putAll(tmp);
    assertEquals(4, map.size());
    assertEquals(map.toString(), "PHashMap {a=1, b=2, c=3, d=4}");
  }

  public void testPutAllUndoRedo() {
    PMap<String, String> map = new PHashMap<String, String>();
    map.put("a", "1");
    map.put("b", "2");
    Map<String, String> tmp = new HashMap<String, String>();
    tmp.put("c", "3");
    tmp.put("d", "4");
    map.putAll(tmp);
    map.undo();
    assertEquals(2, map.size());
    assertEquals(map.toString(), "PHashMap {a=1, b=2}");
    map.redo();
    assertEquals(4, map.size());
    assertEquals(map.toString(), "PHashMap {a=1, b=2, c=3, d=4}");
  }
}
